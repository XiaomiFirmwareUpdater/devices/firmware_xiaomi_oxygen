## Xiaomi Firmware Packages For Mi Max 2 (oxygen)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 328 | MIMAX2 | Xiaomi Mi Max 2 | oxygen |
| 328 | MIMAX2Global | Xiaomi Mi Max 2 Global | oxygen |

### XDA Support Thread For sagit:
[Go here](https://forum.xda-developers.com/mi-max-2/development/firmware-xiaomi-mi-max-2-t3741662)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
